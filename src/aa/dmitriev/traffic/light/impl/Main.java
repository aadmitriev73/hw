package aa.dmitriev.traffic.light.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {

		TrafficLight trafficLight = new TrafficLight();
		System.out.println("Введите время в минутах: ");

		try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
			System.out.println(trafficLight.getColor(Integer.parseInt(reader.readLine())));
		}
	}
}
