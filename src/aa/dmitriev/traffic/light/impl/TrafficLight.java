package aa.dmitriev.traffic.light.impl;

public class TrafficLight {

	private String color;

	TrafficLight(){
	}

	public String getColor (int time){

		if(time<=0){
			return "Время не может быть отрицательным!!";
		}else {
			while(time>=9) {
				time=time-9;
			}
			if(time>=0&&time<2){
				color= "Горит зеленый.";
			}if (time>=2&&time<5){
				color= "Горит желтый.";
			}if (time>=5&&time<9){
				color= "Горит красный.";
			}
		}
		return color;
	}
}
